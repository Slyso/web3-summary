---
geometry: "left=1cm,right=1cm,top=1.5cm,bottom=1.5cm,landscape"
fontsize: 8pt

header-includes:
  - \usepackage{multicol}

classoption:
- twocolumn
...

\thispagestyle{empty}

\begin{LARGE}React\end{LARGE}
\vspace*{-\baselineskip}

**Components** There are many ways to create a component. `props` is always an object and must be treated as read-only.

```javascript
const HelloMessage = (props) => <div>Hello {props.name}</div>;
const HelloMessage = ({ name }) => <div>Hello {name}</div>; // or use `function`
class HelloMessage extends React.Component {
  render() { return <div>Hello {this.props.name}</div> }
}
```

Component state must be managed with `useState`. Variables must not be modified directly but with the `set` method of the respective hook. Otherwise React doesn't properly register the state change. React tries to be resource-efficient by not rendering every single state change on the DOM directly but on a virtual DOM. Based on some heuristics the real DOM is reconciliated (DE: abgestimmt) with the virtual DOM.

```javascript
import { useState } from 'react';
function Counter() {
  const [counter, setCounter] = useState(0);
  const increment = () => setCounter(counter + 1);
  return (<div><p>{counter}</p>
    <button onClick={increment}>Increment Counter</button></div>);
}
```

**useEffect** is used to run side effects in function components. `useEffect(() => {}, [counter, otherStateVariable])` The first parameter is a function causing the side effect, optionally returning a function equivalent to `componentWillUnmount`. The optional 2nd parameter is the dependency array, limiting the function to run only when the provided state has changed, `[]` is equivalent to `componentDidMount`.

![](reactComponentLifecycle.png)

**React Redux toolkit**

```javascript
const balanceSlice = createSlice({ // slices combine state-object, reducer-functions
  name: "balance",                 // and actions into a single concept
  initialState: { value: 0 },
  reducers: {
    transfer: (state, action) => { state.value += action.payload.amount; },
  },
});
export const {transfer} = balanceSlice.actions;
// configure & export store in store.js
import { balanceReducer } from "./redux/balanceSlice";
const store = configureStore({ reducer: { balance: balanceReducer} });
// configure app, import store from above
render(<Provider store={store}><App/></Provider>, document.getElementById('root'));
// somewhere in the state tree, useDispatch & useSelector are from "react-redux"
const dispatch = useDispatch(); dispatch(transfer({ amount: 10 }));
const balance = useSelector(state => state.balance.value);
```

\begin{LARGE}Angular\end{LARGE}
\vspace*{-\baselineskip}

**Component example** x.component.scss for styling, x.component.spec.ts for unit tests, x.component.html for the view, x.component.ts for the logic. Components are contained in modules and are exported/imported through modules. `@Input` fields in components are set by parent in HTML, `@Output` is `EventEmitter` passing data to the parent.

```html
<wed-navigation-bar [type]="'loggedIn'"></wed-navigation-bar>
<p [oneWay]="myVar" [(twoWay)]="myVar" (click)="onClick($event)"><p>
<div id="content">                 <!-- ^^^^^ is marked as @Output in p component-->
  <router-outlet></router-outlet>
  <p>{{user?.accountNr}}</p>
  <ng-container *ngIf="!showTransactionSuccess">
    <form #form="ngForm" (ngSubmit)="transfer(form)">
      <input ngModel (ngModelChange)="foo(target.value)" required #target>
    </form>
  </ng-container>
</div>
```

```typescript
import { Component, OnInit } from '@angular/core';
@Component({  selector: 'my-component',  templateUrl: './x.component.html',  styleUrls: ['./x.component.scss'] })
export class MyComponent implements OnInit {
  public user: Account | null = null;
  constructor(private navigationService: NavigationService) { }
  ngOnInit(): void {  }
  public async transfer(form: NgForm): Promise<boolean> {
    const accountNumber = form.value.target.toString();
    if (!form.valid) return false;
  }
}

@Injectable({providedIn: 'root'}) //modern way with tree shaking (no unused imports)
export class NavigationService {}
// instead of providedIn: 'root' the app module can be annotated:
@NgModule({providers:[NavigationService]}) export class MyModule {}
```

\begin{LARGE}ASP.NET\end{LARGE}
\vspace*{-\baselineskip}

**MVVM** view  ←(data bindings)→ view model (←)→ model (business logic and data) \
view model: value converter, UI logic (e.g. drag & drop)

**Middleware**

```csharp
app.Use(async (context, next) => {
  await next.Invoke(); // call next middleware
});
app.Map("/logging", builder => {
  builder.Run(async context => { /* branch for given URL */ })
});
app.Run(async context => { /* ... */ }); // terminating middleware

class UserMiddleware {
  public UserMiddleware(RequestDelegate next, IUserService s) {} // captive dependency s
  public async Task Invoke(HttpContext context, IUserService s) {} // non-captive dependency s
}
```

**Dependency injection**

```csharp
public class Startup {
  public void ConfigureServices(IServiceCollection services) {
    services.AddMvc(); services.AddTransient<IUserService, UserService>(); // or AddScoped or AddSingleton
  }
  public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
    app.UseMiddleware<UserMiddleware>(); // using class middleware
    app.UseRouting(); // Use auto routing
    app.UseEndpoints(endpoints => {
        endpoints.MapBlazorHub();
        endpoints.MapRazorPages(); // define routing for Razor pages
    });
    app.UseStaticFiles();app.UseRouting();app.UseEndpoints(e => e.MapRazorPages());
  }
}
```

\begin{tabular}{|c|c|}
  Transient & transient lifetime services are create each time they are requested \\
  Scoped    & are created once per request \\
  Singleton & are created the first time they are requested \\
\end{tabular}

This can lead to potential lifetime problems; components must only be injected with components with same or longer lifetimes. (maybe detected by ASP at _runtime_ or unpredictable runtime errors)

**cshtml.cs (view-model)**

```csharp
public class MyModel : PageModel {
  private readonly IUserService _userService;
  public MyModel(IUserService userService) { _userService = userService ; }
  public string SomeText { get; private set; }
  [BindProperty] public FooModel Foo { get; set; } // automatically bound if provided in request
  public void OnGet(int queryParam) { SomeText = "initial value"; }
  public void OnPost(string message, long count) { /* recieve form post */ }
}
```

**Razor syntax (cshtml) (view template syntax)**

```html
@page "/test/{queryParam:int?}" <!-- path optional, but required if query params -->
@model MyModel <!-- link model with view -->
<p>@Model.SomeText, queryParam: @Request.Query["queryParam"]</p>
@{
  var members = new[] {"Hans"};
  @: Number of participants is @members.Length
  <ul>
    @foreach (var m in members) {
      <li style="@(m.StartsWith("M") ? "color:red" : "")">@m</li>
    }
  </ul>

  <!-- forms: trigger OnPost, input -> params -->
  <form asp-page="MyPage">
    <input name="message" placeholder="Message" />
    <input name="count" type="number" placeholder="Count" />
  </form>
}
```

**Built-in input validation**

```csharp
public class Bmi {
  [Range(0, 300)][Display(Name = "foo")] public double Weight { get; set; }
  // usage in a form (cshtml): <label asp-for="@Model.Bmi.Weight"></label>
  // <input asp-for="@Model.Bmi.Weight" name="weight" class="form-control">
}
```

**Routing structure** \*.cshtml (view),  \*.cshtml.cs (view model). /add => pages/add.cshtml, /order/add => pages/order/add.cshtml.

**Tag helpers** Define new HTML tags with the Razor template engine. e.g. `<email mail-for="x"></email>` → `<a href="mailto:x">x</a>`

```csharp
public class EmailTagHelper : TagHelper {
  public string MailFor { get; set; }
  public override void Process(TagHelperContext context, TagHelperOutput output) {
      output.Name = "a";
      output.Attributes.SetAttribute("href", "mailto:" + MailFor);
      output.Content.SetContent(MailFor);
  }
}
```

**View data** Data available in the whole render tree. Available as `Map` in the view, marked with `[ViewData]` in the view-model. e.g. to pass page title to layout: `@{ ViewData["Title"] = "Orders"; }`

**Temp data** Same as view data, but survives redirects and is automatically removed after access.

**REST API**

```csharp
[Route("api/values")
public class ValuesController : Controller {
    // constructor injecting IValueService into _valueService
    [HttpGet] public IEnumerable<Value> Get() { return _valueService.All(); }
    [HttpPost("{id}")] public Value Post(int id, Value value) { return _valueService.Post(id, value); }
}
```
